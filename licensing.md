## DI Software Licensing Policy (WIP)

We will endeavour to make all of the software developed in Design Informatics available publicly under Open Source licences.

By default, software is automatically protected by copyright, so it is important to specify a software licence for your code in order to explicity grant others permission to use it.

The [MIT licence](https://choosealicense.com/licenses/mit/) is generally a good starting point, as it has very few restrictions. However, research funders may have specific requirements. You can use [choosealicense.com](https://choosealicense.com/) to select a licence that fits your requirements.

