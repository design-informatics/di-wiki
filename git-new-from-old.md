# Git: New from Old
This enables you to create a new repository from a sub-section of an existing repository, whilst ensuring that all unwanted files and folders are entirely removed from the new repository and its history. Useful if you want to save space or remove sensitive data.

## Instructions

### Step 1
We're going to use a tool called [git-filter-repo](https://github.com/newren/git-filter-repo), which is recommended as an alternative to the [git-filter-branch](https://git-scm.com/docs/git-filter-branch) tool included with git. To install git-filter-repo on a Mac, use pip (requires Python 3):  `pip3 install git-filter-repo `. Other installation methods can be found [here](https://github.com/newren/git-filter-repo/blob/master/INSTALL.md).

## Step 2
Clone a copy of the repository that you want to modify, e.g. `git clone git@git.ecdf.ed.ac.uk:design-informatics/my-project.git`.

Make a local copy of this repository, e.g. `cp -r my-project/ my-project-copy`.

Change to the directory of your copied project, e.g. `cd my-project-copy/`.

## Step 3
Use git-filter-repo to preserve the files/folders you want to keep, and delete the rest. The basic command is `git filter-repo --path keepthis`, where `keepthis` is the file or folder you want to keep. You can chain multiple files and folders, e.g. `git filter-repo --path keepthisfile --path keepthisfolder/`. Or you can put all the files/folders you want to keep in a text file (e.g. stuff-to-keep.txt) and use `--paths-from-file`, e.g. `git filter-repo --paths-from-file stuff-to-keep.txt`. The contents of stuff-to-keep.txt might look like this:
```
keepthisfile
keepthisfolder/
```
**git-filter-repo works across all branches in your repository, so remember to include files and folders that you want to keep in branches other than the currently checked out branch**.

------------
**Note**: if you get the following error then you will need to update your version of git: `Error: need a version of git whose diff-tree command has the --combined-all-paths option`. I used homebrew to update git and then switched to use the homebrew version of git:
```
brew install git
export PATH="/usr/local/bin:${PATH}"
source ~/.bash_profile
```
---------

## Step 4
Push your newly pruned repository to a new remote location, e.g.:
```
git remote add origin git@git.ecdf.ed.ac.uk:design-informatics/my-new-project.git
git push origin –-all
```
